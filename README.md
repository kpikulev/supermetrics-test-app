# Supermetrics test app

## Requirements
You need to use Node v20.16.0

## How to start application:

### DEV mode

1. Copy `packages/backend/config.example.yaml` to `packages/backend/config.yaml`
2. Run `docker-compose up`
3. After steps above, you can open http://localhost:4000
4. Also, you can install node_modules for IDE (start commands from project root):
    ```shell
    npm install
    cd packages/shared
    npm install
    cd ../backend
    npm install
    cd ../frontend
    npm install
    ```

### PROD mode

1. Copy `packages/backend/config.example.yaml` to `packages/backend/config.yaml`
2. Run `docker-compose -f docker-compose.prod.yaml up`
3. After steps above, you can open http://localhost:4000

## Work with migrations

1) Open container's bash
```shell
docker exec -it supermetrics-test-app-backend bash
```
2) Migration generate:
```shell
/app$ npm run typeorm -- migration:generate ./src/database/migrations/MigrationName
```
3) Migration revert
```shell
/app$ npm run typeorm migration:revert
```
