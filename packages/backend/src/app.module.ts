import { Module } from "@nestjs/common";
import { EventEmitterModule } from "@nestjs/event-emitter";

import { ConfigModule } from "~/app/config/config.module";
import { PostModule } from "~/app/post/post.module";
import { DatabaseModule } from "~/database/database.module";
import { UserModule } from "~/app/user/user.module";

@Module({
  imports: [ConfigModule, DatabaseModule, EventEmitterModule.forRoot({ global: true }), PostModule, UserModule]
})
export class AppModule {}
