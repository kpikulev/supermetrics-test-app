import { Module } from "@nestjs/common";

import { ConfigService } from "~/app/config/config.service";

@Module({
  exports: [ConfigService],
  providers: [ConfigService]
})
export class ConfigModule {}
