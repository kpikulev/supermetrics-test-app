import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Entity("posts")
@Index(["postOriginalId"], { unique: true })
export class PostEntity {
  @PrimaryGeneratedColumn("uuid", { name: "id" })
  readonly id: string;

  @Column({ type: "text", name: "post_original_id" })
  readonly postOriginalId: string;

  @Column({ type: "text", name: "from_name" })
  readonly fromName: string;

  @Column({ type: "text", name: "from_id" })
  readonly fromId: string;

  @Column({ type: "text", name: "message" })
  readonly message: string;

  @Column({ type: "text", name: "type" })
  readonly type: string;

  @Column({ type: "timestamp with time zone", name: "created_time" })
  readonly createdTime: Date;
}
