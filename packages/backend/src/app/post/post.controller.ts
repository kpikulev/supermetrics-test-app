import { Controller, Get, Query } from "@nestjs/common";
import { POST_ROUTES, PostBackendAPI, PostsResponse } from "@template/shared/post/post.api";

import { PostService } from "./post.service";

@Controller()
export class PostController implements PostBackendAPI {
  constructor(private readonly postService: PostService) {}

  @Get(POST_ROUTES.getPosts())
  async getPosts(@Query("perPage") perPage: string, @Query("page") page: string): Promise<PostsResponse> {
    return await this.postService.getPage(Number(perPage), Number(page));
  }
}
