import { Module } from "@nestjs/common";
import { HttpModule } from "@nestjs/axios";

import { PostService } from "./post.service";
import { PostController } from "./post.controller";

import { ConfigService } from "~/app/config/config.service";

@Module({
  imports: [HttpModule],
  controllers: [PostController],
  providers: [PostService, ConfigService],
  exports: [ConfigService, PostService, HttpModule]
})
export class PostModule {}
