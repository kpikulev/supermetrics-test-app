import { HttpException, HttpStatus, Injectable, Logger } from "@nestjs/common";
import { DataSource, FindOneOptions, QueryRunner, Repository } from "typeorm";
import { HttpService } from "@nestjs/axios";
import { firstValueFrom } from "rxjs";
import { AxiosResponse } from "axios";
import { PostsResponse, SupermetricsPostFromResponse, SupermetricsPostsResponse } from "@template/shared/post/post.api";
import { UserStatisticsResponse } from "@template/shared/user/user.api";

import { PostEntity } from "~/app/post/entities/post.entity";
import { ConfigService } from "~/app/config/config.service";
import { makePostFromPostEntity } from "~/app/post/post.utils";

@Injectable()
export class PostService {
  private readonly logger: Logger = new Logger(PostService.name);

  constructor(
    private readonly appDataSource: DataSource,
    private readonly httpService: HttpService,
    private readonly configService: ConfigService
  ) {}

  async onModuleInit(): Promise<void> {
    this.logger.log("Posts fetching was started");
    const queryRunner: QueryRunner = this.appDataSource.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      await this.fetchAndSavePostsFromSupermetrics(1, queryRunner);
      await queryRunner.commitTransaction();
    } catch (e) {
      await queryRunner.rollbackTransaction();
      throw e;
    } finally {
      await queryRunner.release();
    }
    this.logger.log("Posts fetching was finished");
  }

  async getPage(perPage: number, page: number): Promise<PostsResponse> {
    const postRepository: Repository<PostEntity> = this.appDataSource.getRepository(PostEntity);
    const skip: number = perPage * page - perPage;

    const result: [PostEntity[], number] = await postRepository.findAndCount({
      take: perPage,
      skip
    });

    return {
      posts: result[0].map(postEntity => makePostFromPostEntity(postEntity)),
      postsPerPage: perPage,
      allPostsCount: result[1],
      currentPage: page
    };
  }

  async getUserStatistics(id: string): Promise<UserStatisticsResponse> {
    const postRepository: Repository<PostEntity> = this.appDataSource.getRepository(PostEntity);

    const numberOfPosts: number = await postRepository.count({ where: { fromId: id } });
    const [{ median: medianNumberOfCharacters }]: [{ median: number }] = (await postRepository.query(
      "SELECT PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY char_length(message)) AS median FROM posts WHERE from_id = $1",
      [id]
    )) as [{ median: number }];

    const postsCountPerMonth: { month: string; posts_count: string }[] = (await postRepository.query(
      "SELECT EXTRACT(MONTH FROM created_time) as month, COUNT(id) AS posts_count FROM posts WHERE from_id = $1 GROUP BY month",
      [id]
    )) as { month: string; posts_count: string }[];

    const [longestPost]: [{ message: string; max_length: string }] = (await postRepository.query(
      "SELECT message, char_length(message) AS max_length FROM posts WHERE from_id = $1 ORDER BY max_length DESC LIMIT 1",
      [id]
    )) as [{ message: string; max_length: string }];

    return {
      userStatistics: {
        numberOfPosts,
        medianNumberOfCharacters,
        postsCountPerMonth,
        longestPost
      }
    };
  }

  async findOne(options: FindOneOptions<PostEntity>): Promise<PostEntity> {
    const PostRepository: Repository<PostEntity> = this.appDataSource.getRepository(PostEntity);

    const post: PostEntity | null = await PostRepository.findOne(options);
    if (!post) {
      throw new HttpException("Not found", HttpStatus.NOT_FOUND);
    }

    return post;
  }

  async fetchAndSavePostsFromSupermetrics(page: number, queryRunner: QueryRunner): Promise<boolean> {
    const url: URL = new URL("https://api.supermetrics.com/assignment/posts");
    url.searchParams.set("sl_token", this.configService.slToken);
    url.searchParams.set("page", String(page));

    try {
      const postsResponse: AxiosResponse<SupermetricsPostsResponse> = await firstValueFrom(
        this.httpService.get(url.toString())
      );

      const posts: SupermetricsPostFromResponse[] = postsResponse.data.data.posts;

      if (page > postsResponse.data.data.page) {
        return true;
      }

      for (const post of posts) {
        const postEntity: PostEntity = queryRunner.manager.create(PostEntity, {
          postOriginalId: post.id,
          fromName: post.from_name,
          fromId: post.from_id,
          message: post.message,
          type: post.type,
          createdTime: post.created_time
        });
        await queryRunner.manager.save(postEntity);
      }

      await this.fetchAndSavePostsFromSupermetrics(page + 1, queryRunner);

      return true;
    } catch (e) {
      return false;
    }
  }
}
