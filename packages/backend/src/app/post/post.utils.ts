import { Post, PostDraft } from "@template/shared/post/post.api";
import { makePostDraftFromPost } from "@template/shared/post/post.utils";

import { PostEntity } from "~/app/post/entities/post.entity";

export const makePostFromPostEntity: (postEntity: PostEntity) => Post = postEntity => {
  const { id, postOriginalId, fromName, fromId, message, type, createdTime }: PostEntity = postEntity;

  return {
    id,
    postOriginalId,
    fromName,
    fromId,
    message,
    type,
    createdTime
  };
};

export const makePostDraftFromPostEntity: (postEntity: PostEntity) => PostDraft = postEntity => {
  return makePostDraftFromPost(makePostFromPostEntity(postEntity));
};
