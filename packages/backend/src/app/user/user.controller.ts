import { Controller, Get, Param } from "@nestjs/common";
import { USER_ROUTES, UserBackendAPI, UserStatisticsResponse } from "@template/shared/user/user.api";

import { PostService } from "~/app/post/post.service";

@Controller()
export class UserController implements UserBackendAPI {
  constructor(private readonly postService: PostService) {}

  @Get(USER_ROUTES.getUserStatistics(":id"))
  async getUserStatistics(@Param("id") id: string): Promise<UserStatisticsResponse> {
    return await this.postService.getUserStatistics(id);
  }
}
