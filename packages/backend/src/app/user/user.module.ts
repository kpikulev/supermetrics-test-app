import { Module } from "@nestjs/common";
import { HttpModule } from "@nestjs/axios";

import { UserController } from "~/app/user/user.controller";
import { PostModule } from "~/app/post/post.module";

@Module({
  imports: [HttpModule, PostModule],
  controllers: [UserController]
})
export class UserModule {}
