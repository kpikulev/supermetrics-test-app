import { ExecutionContext, PipeTransform, createParamDecorator } from "@nestjs/common";
import { Type } from "@nestjs/common/interfaces";

import { Request } from "~/common/types/request";

export const CurrentUser: (...dataOrPipes: (Type<PipeTransform> | PipeTransform)[]) => ParameterDecorator =
  createParamDecorator((_: any, context: ExecutionContext) => {
    const request: Request = context.switchToHttp().getRequest<Request>();
    return request.user;
  });
