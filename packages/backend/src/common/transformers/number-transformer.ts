import { ValueTransformer } from "typeorm";

export const NUMBER_TRANSFORMER: ValueTransformer = {
  from(value?: string): number | null {
    return value ? Number(value) : null;
  },
  to(value?: number | null): string | null {
    return value === undefined || value === null ? null : value.toString();
  }
};
