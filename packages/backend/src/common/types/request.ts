import { Request as RawRequest } from "express";
import { UserInfo } from "@template/shared/auth/user-info";

export interface Request extends RawRequest {
  user: UserInfo;
}
