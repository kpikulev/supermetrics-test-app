import { DataSource } from "typeorm";

import { staticConfig } from "~/app/config/config.service";

export const AppDataSource: DataSource = new DataSource({
  type: "postgres",
  host: staticConfig.database.host,
  port: staticConfig.database.port,
  username: staticConfig.database.username,
  password: staticConfig.database.password,
  database: staticConfig.database.database,
  entities: [__dirname + "/../app/**/*.entity{.ts,.js}"],
  migrations: [__dirname + "/migrations/**/*{.ts,.js}"],
  synchronize: false,
  migrationsRun: true,
  logging: true
});
