import { MigrationInterface, QueryRunner } from "typeorm";

export class Posts1668167082366 implements MigrationInterface {
  name: string = "Posts1668167082366";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "posts" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "post_original_id" text NOT NULL, "from_name" text NOT NULL, "from_id" text NOT NULL, "message" text NOT NULL, "type" text NOT NULL, "created_time" TIMESTAMP WITH TIME ZONE NOT NULL, CONSTRAINT "PK_2829ac61eff60fcec60d7274b9e" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(`CREATE UNIQUE INDEX "IDX_42bf7c407b0c8ba07b385a1557" ON "posts" ("post_original_id") `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP INDEX "public"."IDX_42bf7c407b0c8ba07b385a1557"`);
    await queryRunner.query(`DROP TABLE "posts"`);
  }
}
