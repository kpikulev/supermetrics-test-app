import "reflect-metadata";
import { CorsOptionsCallback } from "@nestjs/common/interfaces/external/cors-options.interface";
import { addAlias } from "module-alias";
addAlias("~", __dirname);
import { INestApplication } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";
import { Request } from "express";
import { useContainer } from "class-validator";
import cookieParser from "cookie-parser";
import { json, urlencoded } from "body-parser";

import { AppModule } from "~/app.module";
import { ConfigService } from "~/app/config/config.service";

async function bootstrap(): Promise<void> {
  const app: INestApplication = await NestFactory.create(AppModule, {});
  useContainer(app.select(AppModule), { fallbackOnErrors: true });
  app.use(cookieParser());
  app.use(json({ limit: "50mb" }));
  app.use(urlencoded({ limit: "50mb", extended: true }));
  const configService: ConfigService = app.get(ConfigService);
  if (configService.corsEnabled) {
    const error: any = undefined;
    app.enableCors((req: Request, cb: CorsOptionsCallback): void =>
      cb(error as Error, {
        origin: req.get("origin"),
        credentials: true,
        exposedHeaders: ["Location"]
      })
    );
  }
  await app.listen(3000);
  console.info("Server up and running...");
}

bootstrap().catch((e: Error) => console.error(e));
