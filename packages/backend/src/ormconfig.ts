import { staticConfig } from "~/app/config/config.service";

export = {
  type: "postgres",
  host: staticConfig.database.host,
  port: staticConfig.database.port,
  username: staticConfig.database.username,
  password: staticConfig.database.password,
  database: staticConfig.database.database,
  entities: ["src/app/**/*.entity{.ts,.js}"],
  migrations: ["src/database/migrations/**/*{.ts,.js}"],
  cli: {
    migrationsDir: "src/database/migrations"
  },
  synchronize: false,
  migrationsRun: true,
  logging: true
};
