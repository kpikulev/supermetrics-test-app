import { Post } from "@template/shared/post/post.api";

import { actionTypes } from "~/interfaces";
import * as actionIs from "~/interfaces/actions.interfaces";

export function failure(error: Error): actionIs.Failure {
  return {
    type: actionTypes.FAILURE,
    error
  };
}

export function fetchPosts(): actionIs.FetchPosts {
  return {
    type: actionTypes.FETCH_POSTS
  };
}

export function setPosts(data: Post[]): actionIs.SetPosts {
  return {
    type: actionTypes.SET_POSTS,
    data
  };
}
