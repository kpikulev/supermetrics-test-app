import React from "react";

import styles from "./styles.module.scss";

import { HeaderMenu } from "~/components/Header/HeaderMenu";
import { HeaderLogo } from "~/components/Header/HeaderLogo";

interface HeaderProps {
  light: boolean;
}

export const Header: React.FC<HeaderProps> = ({ light }: HeaderProps) => {
  console.info(light);
  return (
    <div className={styles.headerContainer}>
      <HeaderLogo />
      <HeaderMenu />
    </div>
  );
};
