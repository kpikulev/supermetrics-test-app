import React from "react";
import { createSelector, Selector } from "reselect";
import { useSelector } from "react-redux";
import Link from "next/link";

import styles from "./styles.module.scss";

import { ApplicationStore } from "~/store";

interface PostsProps {
  light: boolean;
}

type SelectData = Pick<ApplicationStore, "posts">;
const dataSelector: Selector<ApplicationStore, SelectData> = createSelector(
  (state: ApplicationStore) => state.posts,
  posts => ({ posts })
);

export const Posts: React.FC<PostsProps> = ({ light }: PostsProps) => {
  console.info(light);
  const selectedData: SelectData = useSelector<ApplicationStore, SelectData>(dataSelector);
  const { posts }: SelectData = selectedData;

  return (
    <div className={styles.postsContainer}>
      {posts.postsData.posts.map(post => {
        return (
          <div key={post.id} className={styles.postsContainerPost}>
            <div>
              <div className={styles.postsContainerPostTitle}>Id:</div>
              <div className={styles.postsContainerPostValue}>{post.id}</div>
            </div>
            <div>
              <div className={styles.postsContainerPostTitle}>From:</div>
              <div className={styles.postsContainerPostValue}>
                <Link href={`/person/${post.fromId}`} target="_blank">
                  {post.fromName}
                </Link>
              </div>
            </div>
            <div className={styles.postsContainerPostMessage}>{post.message}</div>
          </div>
        );
      })}
    </div>
  );
};
