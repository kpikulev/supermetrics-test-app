declare namespace StylesModuleScssNamespace {
  export interface IStylesModuleScss {
    postsContainer: string;
    postsContainerPost: string;
    postsContainerPostMessage: string;
    postsContainerPostTitle: string;
    postsContainerPostValue: string;
  }
}

declare const StylesModuleScssModule: StylesModuleScssNamespace.IStylesModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: StylesModuleScssNamespace.IStylesModuleScss;
};

export = StylesModuleScssModule;
