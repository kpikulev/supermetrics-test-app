import React from "react";
import { createSelector, Selector } from "reselect";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import classNames from "classnames";

import styles from "./styles.module.scss";

import { ApplicationStore } from "~/store";
import { fetchPosts } from "~/components/Post/store/actions";

type SelectData = Pick<ApplicationStore, "posts">;
const dataSelector: Selector<ApplicationStore, SelectData> = createSelector(
  (state: ApplicationStore) => state.posts,
  posts => ({ posts })
);

export const PostsPagination: React.FC = () => {
  const selectedData: SelectData = useSelector<ApplicationStore, SelectData>(dataSelector);
  const { posts }: SelectData = selectedData;
  const dispatch: Dispatch = useDispatch();

  console.info("___ allPostsCount ___");
  console.info(posts.postsData.allPostsCount);
  console.info("___ postsPerPage ___");
  console.info(posts.postsData.postsPerPage);
  console.info("___ currentPage ___");
  console.info(posts.postsData.currentPage);

  const pagesCount: number = posts.postsData.allPostsCount / posts.postsData.postsPerPage;

  const pagesArray: number[] = [];
  for (let x: number = 1; x <= pagesCount; x++) {
    pagesArray.push(x);
  }

  return (
    <div className={styles.postsPaginationContainer}>
      {pagesArray.map(pageNumber => (
        <div
          className={classNames(styles.postsPaginationContainerPage, {
            [styles.postsPaginationContainerPageActive]: pageNumber === posts.postsData.currentPage
          })}
          key={pageNumber}
          onClick={(): void => {
            dispatch(fetchPosts({ page: pageNumber }));
          }}
        >
          {pageNumber}
        </div>
      ))}
    </div>
  );
};
