import { ActionCreatorBuilder, createAction } from "typesafe-actions";
import { PostsResponse, FetchPostsRequest } from "@template/shared/post/post.api";
import { HYDRATE } from "next-redux-wrapper";

import { ApplicationStore } from "~/store";

export const fetchPosts: ActionCreatorBuilder<"posts/fetchPosts", FetchPostsRequest> =
  createAction("posts/fetchPosts")<FetchPostsRequest>();

export const setPosts: ActionCreatorBuilder<"posts/setPosts", PostsResponse> =
  createAction("posts/setPosts")<PostsResponse>();

export const postsHydrate: ActionCreatorBuilder<string, ApplicationStore> = createAction(HYDRATE)<ApplicationStore>();
