import { Action, combineReducers, Reducer } from "redux";
import { createReducer, PayloadAction } from "typesafe-actions";
import { Post, PostsResponse } from "@template/shared/post/post.api";

import { postsHydrate, setPosts } from "~/components/Post/store/actions";
import { ApplicationStore } from "~/store";

export interface PostsStore {
  postsData: {
    posts: Post[];
    postsPerPage: number;
    allPostsCount: number;
    currentPage: number;
  };
}

export const postsReducer: Reducer<PostsStore> = combineReducers({
  postsData: createReducer<PostsStore["postsData"], Action>({
    posts: [],
    postsPerPage: 0,
    allPostsCount: 0,
    currentPage: 0
  })
    .handleAction(setPosts, (_: PostsStore["postsData"], { payload }: PayloadAction<string, PostsResponse>) => {
      return {
        posts: payload.posts,
        postsPerPage: payload.postsPerPage,
        allPostsCount: payload.allPostsCount,
        currentPage: payload.currentPage
      };
    })
    .handleAction(postsHydrate, (_: PostsStore["postsData"], { payload }: PayloadAction<string, ApplicationStore>) => {
      return payload.posts.postsData;
    })
});
