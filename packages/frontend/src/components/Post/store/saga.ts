import { call } from "typed-redux-saga";
import { PostsResponse, FetchPostsRequest } from "@template/shared/post/post.api";
import { Effect, put, takeLatest } from "redux-saga/effects";
import { PayloadAction } from "typesafe-actions";

import { postService } from "~/service/post.service";
import { failure } from "~/actions";
import { fetchPosts, setPosts } from "~/components/Post/store/actions";

function* fetchPostsSaga(action: PayloadAction<"posts/fetchPosts", FetchPostsRequest>): Generator {
  try {
    const postsResponse: PostsResponse = yield* call(() => postService.getPosts(20, action.payload.page));
    yield put(setPosts(postsResponse));
  } catch (err) {
    const error: Error = err as Error;
    yield put(failure(error));
  }
}

export function* postSaga(): Generator<Effect | Generator, void, void> {
  yield takeLatest(fetchPosts, fetchPostsSaga);
}
