import React from "react";
import { createSelector, Selector } from "reselect";
import { useSelector } from "react-redux";

import styles from "./styles.module.scss";

import { ApplicationStore } from "~/store";

type SelectData = Pick<ApplicationStore, "users">;
const dataSelector: Selector<ApplicationStore, SelectData> = createSelector(
  (state: ApplicationStore) => state.users,
  users => ({ users })
);

export const User: React.FC = () => {
  const selectedData: SelectData = useSelector<ApplicationStore, SelectData>(dataSelector);
  const { users }: SelectData = selectedData;

  return (
    <div className={styles.userContainer}>
      <div className={styles.userContainerTitle}>{users.userId} statistics:</div>
      <div className={styles.userContainerItem}>
        <div className={styles.userContainerField}>Number of posts:</div>
        <div className={styles.userContainerValue}>{users.userStatistics.numberOfPosts}</div>
      </div>
      <div className={styles.userContainerItem}>
        <div className={styles.userContainerField}>The median number of characters:</div>
        <div className={styles.userContainerValue}>{users.userStatistics.medianNumberOfCharacters}</div>
      </div>
      <div className={styles.userContainerItem}>
        <div className={styles.userContainerField}>The number of posts per month:</div>
        <div className={styles.userContainerValue}>
          {users.userStatistics.postsCountPerMonth.map(monthData => (
            <div key={monthData.month}>
              In month number {monthData.month} was {monthData.posts_count} posts
            </div>
          ))}
        </div>
      </div>
      <div className={styles.userContainerItem}>
        <div className={styles.userContainerField}>The longest post:</div>
        <div className={styles.userContainerValue}>
          <strong>Length: {users.userStatistics.longestPost?.max_length}, post text:</strong>
          <br />
          {users.userStatistics.longestPost?.message}
        </div>
      </div>
    </div>
  );
};
