declare namespace StylesModuleScssNamespace {
  export interface IStylesModuleScss {
    userContainer: string;
    userContainerField: string;
    userContainerItem: string;
    userContainerTitle: string;
    userContainerValue: string;
  }
}

declare const StylesModuleScssModule: StylesModuleScssNamespace.IStylesModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: StylesModuleScssNamespace.IStylesModuleScss;
};

export = StylesModuleScssModule;
