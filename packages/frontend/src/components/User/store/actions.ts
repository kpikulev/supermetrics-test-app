import { ActionCreatorBuilder, createAction } from "typesafe-actions";
import { HYDRATE } from "next-redux-wrapper";
import { UserStatisticsResponse } from "@template/shared/user/user.api";

import { ApplicationStore } from "~/store";

export const setUserId: ActionCreatorBuilder<"users/setUserId", string> = createAction("users/setUserId")<string>();

export const fetchUserStatistics: ActionCreatorBuilder<"users/fetchUserStatistics", string> =
  createAction("users/fetchUserStatistics")<string>();

export const setUserStatistics: ActionCreatorBuilder<"users/setUserStatistics", UserStatisticsResponse> =
  createAction("users/setUserStatistics")<UserStatisticsResponse>();

export const usersHydrate: ActionCreatorBuilder<string, ApplicationStore> = createAction(HYDRATE)<ApplicationStore>();
