import { Action, combineReducers, Reducer } from "redux";
import { createReducer, PayloadAction } from "typesafe-actions";
import { UserStatisticsResponse } from "@template/shared/user/user.api";

import { usersHydrate, setUserStatistics, setUserId } from "~/components/User/store/actions";
import { ApplicationStore } from "~/store";

export interface UsersStore {
  userId: string;
  userStatistics: {
    numberOfPosts: number;
    medianNumberOfCharacters: number;
    postsCountPerMonth: { month: string; posts_count: string }[];
    longestPost: { message: string; max_length: string } | null;
  };
}

export const usersReducer: Reducer<UsersStore> = combineReducers({
  userId: createReducer<UsersStore["userId"], Action>("")
    .handleAction(setUserId, (_: UsersStore["userId"], { payload }: PayloadAction<string, string>) => {
      return payload;
    })
    .handleAction(usersHydrate, (_: UsersStore["userId"], { payload }: PayloadAction<string, ApplicationStore>) => {
      return payload.users.userId;
    }),
  userStatistics: createReducer<UsersStore["userStatistics"], Action>({
    numberOfPosts: 0,
    medianNumberOfCharacters: 0,
    postsCountPerMonth: [],
    longestPost: null
  })
    .handleAction(
      setUserStatistics,
      (_: UsersStore["userStatistics"], { payload }: PayloadAction<string, UserStatisticsResponse>) => {
        return {
          numberOfPosts: payload.userStatistics.numberOfPosts,
          medianNumberOfCharacters: payload.userStatistics.medianNumberOfCharacters,
          postsCountPerMonth: payload.userStatistics.postsCountPerMonth,
          longestPost: payload.userStatistics.longestPost
        };
      }
    )
    .handleAction(
      usersHydrate,
      (_: UsersStore["userStatistics"], { payload }: PayloadAction<string, ApplicationStore>) => {
        return payload.users.userStatistics;
      }
    )
});
