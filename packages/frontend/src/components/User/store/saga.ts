import { call } from "typed-redux-saga";
import { Effect, put, takeLatest } from "redux-saga/effects";
import { PayloadAction } from "typesafe-actions";
import { UserStatisticsResponse } from "@template/shared/user/user.api";

import { userService } from "~/service/user.service";
import { failure } from "~/actions";
import { fetchUserStatistics, setUserId, setUserStatistics } from "~/components/User/store/actions";

function* fetchUserStatisticsSaga(action: PayloadAction<"users/fetchUserStatistics", string>): Generator {
  try {
    const userStatisticsResponse: UserStatisticsResponse = yield* call(() =>
      userService.getUserStatistics(action.payload)
    );
    yield put(setUserId(action.payload));
    yield put(setUserStatistics(userStatisticsResponse));
  } catch (err) {
    const error: Error = err as Error;
    yield put(failure(error));
  }
}

export function* userSaga(): Generator<Effect | Generator, void, void> {
  yield takeLatest(fetchUserStatistics, fetchUserStatisticsSaga);
}
