import { Post } from "@template/shared/post/post.api";

export enum actionTypes {
  FAILURE = "FAILURE",
  FETCH_POSTS = "FETCH_POSTS",
  SET_POSTS = "SET_POSTS"
}

export type Action = Failure | FetchPosts | SetPosts;

export interface Failure {
  type: actionTypes.FAILURE;
  error: Error;
}

export interface FetchPosts {
  type: actionTypes.FETCH_POSTS;
}

export interface SetPosts {
  type: actionTypes.SET_POSTS;
  data: Post[];
}
