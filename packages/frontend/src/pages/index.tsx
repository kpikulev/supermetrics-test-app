import React from "react";
import { NextPage } from "next";

import { Header } from "~/components/Header";
import styles from "~/styles/index.module.scss";
import { Page } from "~/components/Page";

const Index: NextPage = () => {
  return (
    <div className={styles.indexPage}>
      <Page>
        <div className={styles.indexPageBg} />
        <div className={styles.indexPageBg2} />
        <Header light={true} />
        <h1>Supermetrics test app</h1>
        <p>Here, I want to show you my best practice of using NestJS + Next.js.</p>
        <p>I hope, you have just easily started this application using Docker :)</p>
        <p>All the photos are taken by me.</p>
      </Page>
    </div>
  );
};

export default Index;
