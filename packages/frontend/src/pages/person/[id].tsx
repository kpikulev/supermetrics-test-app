import React from "react";
import { GetServerSideProps, GetServerSidePropsResult, NextPage } from "next";
import { END } from "redux-saga";

import styles from "~/styles/person.module.scss";
import { Page } from "~/components/Page";
import { wrapper } from "~/store";
import { Header } from "~/components/Header";
import { fetchUserStatistics } from "~/components/User/store/actions";
import { User } from "~/components/User/Index";

const Index: NextPage = () => {
  return (
    <div className={styles.personPage}>
      <Page>
        <div className={styles.personPageBg} />
        <Header light={true} />

        <User />
      </Page>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = wrapper.getServerSideProps(
  store =>
    async (context): Promise<GetServerSidePropsResult<{ a: "d" }>> => {
      const userId: string = String(context.query.id);
      store.dispatch(fetchUserStatistics(userId));
      store.dispatch(END);
      return await store.sagaTask.toPromise();
    }
);

export default Index;
