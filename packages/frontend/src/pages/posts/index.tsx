import React from "react";
import { GetStaticProps, GetStaticPropsResult, NextPage } from "next";
import { END } from "redux-saga";

import styles from "~/styles/posts.module.scss";
import { Page } from "~/components/Page";
import { wrapper } from "~/store";
import { Header } from "~/components/Header";
import { Posts } from "~/components/Post/Posts";
import { fetchPosts } from "~/components/Post/store/actions";
import { PostsPagination } from "~/components/Post/PostsPagination";

const Index: NextPage = () => {
  // const dispatch: Dispatch = useDispatch();
  //
  // useEffect(() => {
  //   dispatch(startClock());
  // });

  return (
    <div className={styles.postsPage}>
      <Page>
        <div className={styles.postsPageBg} />
        <Header light={true} />

        <PostsPagination />
        <Posts light={true} />
        <PostsPagination />
      </Page>
    </div>
  );
};

export const getServerSideProps: GetStaticProps = wrapper.getStaticProps(
  store => async (): Promise<GetStaticPropsResult<{ a: "d" }>> => {
    store.dispatch(fetchPosts({ page: 1 }));

    store.dispatch(END);
    return await store.sagaTask.toPromise();
  }
);

export default Index;
