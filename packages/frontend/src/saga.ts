import { Effect, fork } from "redux-saga/effects";
import { all } from "typed-redux-saga";

import { postSaga } from "~/components/Post/store/saga";
import { userSaga } from "~/components/User/store/saga";

export default function* rootSaga(): Generator<Effect | Generator, void, void> {
  yield all([fork(postSaga), fork(userSaga)]);
}
