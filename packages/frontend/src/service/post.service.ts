import { POST_ROUTES, PostFrontendAPI, PostsResponse } from "@template/shared/post/post.api";

import { apiFetch } from "~/utils/api-fetch";

class PostService implements PostFrontendAPI {
  getPosts(perPage: number, page: number): Promise<PostsResponse> {
    return apiFetch(POST_ROUTES.getPosts(), {
      queryParams: {
        perPage,
        page
      }
    });
  }
}

export const postService: PostService = new PostService();
