import { USER_ROUTES, UserFrontendAPI, UserStatisticsResponse } from "@template/shared/user/user.api";

import { apiFetch } from "~/utils/api-fetch";

class UserService implements UserFrontendAPI {
  getUserStatistics(id: string): Promise<UserStatisticsResponse> {
    return apiFetch(USER_ROUTES.getUserStatistics(id));
  }
}

export const userService: UserService = new UserService();
