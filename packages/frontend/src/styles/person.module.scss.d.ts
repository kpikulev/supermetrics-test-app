declare namespace PersonModuleScssNamespace {
  export interface IPersonModuleScss {
    personPage: string;
    personPageBg: string;
  }
}

declare const PersonModuleScssModule: PersonModuleScssNamespace.IPersonModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: PersonModuleScssNamespace.IPersonModuleScss;
};

export = PersonModuleScssModule;
