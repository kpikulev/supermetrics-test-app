import PlainObject = JQuery.PlainObject;

export function iterateObject(object: PlainObject, callback: (key: string, value: unknown, i: number) => void): void {
  let i: number = 0;

  Object.keys(object).map(key => {
    const value: unknown = object[key];
    callback(key, value, i);
    i++;
  });
}
