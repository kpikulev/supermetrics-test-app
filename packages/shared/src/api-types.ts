export type ApiError = { status: number; description: { [key: string]: string } };
export type ApiResponse<T> = { payload: T | undefined; error: ApiError | undefined };
