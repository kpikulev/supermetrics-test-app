import { APIRoutes } from "../api-routes";

export class PostDraft {
  postOriginalId: string;

  fromName: string;

  fromId: string;

  message: string;

  type: string;

  createdTime: Date;
}

export class Post extends PostDraft {
  id: string;
}

export interface PostApi {
  getPosts(...omit: any[]): unknown;
}

export interface SupermetricsPostFromResponse {
  id: string;
  from_name: string;
  from_id: string;
  message: string;
  type: string;
  created_time: string;
}

export interface SupermetricsPostsResponse {
  meta: { request_id: string };
  data: {
    page: number;
    posts: SupermetricsPostFromResponse[];
  };
}

export interface PostsResponse {
  posts: Post[];
  postsPerPage: number;
  allPostsCount: number;
  currentPage: number;
}

export interface FetchPostsRequest {
  page: number;
}

type Modify<T, R extends T> = Omit<T, keyof R> & R;

export type PostFrontendAPI = Pick<Modify<PostApi, PostFrontendAPIStructure>, keyof PostApi>;
interface PostFrontendAPIStructure {
  getPosts(...omit: any[]): Promise<PostsResponse>;
}

export type PostBackendAPI = Pick<Modify<PostApi, PostBackendAPIStructure>, keyof PostApi>;
interface PostBackendAPIStructure {
  getPosts(...omit: any[]): Promise<PostsResponse>;
}

export const POST_ROUTES: APIRoutes<PostApi> = {
  getPosts: () => "/posts"
};
