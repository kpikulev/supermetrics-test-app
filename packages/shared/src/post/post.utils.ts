import { Post, PostDraft } from "./post.api";

export const makePostDraftFromPost: (post: Post) => PostDraft = post => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { id, ...rest }: Post = post;
  return rest;
};
