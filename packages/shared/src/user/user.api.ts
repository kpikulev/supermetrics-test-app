import { APIRoutes } from "../api-routes";

export interface UserApi {
  getUserStatistics(...omit: any[]): unknown;
}

type Modify<T, R extends T> = Omit<T, keyof R> & R;

export interface UserStatisticsResponse {
  userStatistics: {
    numberOfPosts: number;
    medianNumberOfCharacters: number;
    postsCountPerMonth: { month: string; posts_count: string }[];
    longestPost: { message: string; max_length: string } | null;
  };
}

export type UserFrontendAPI = Pick<Modify<UserApi, UserFrontendAPIStructure>, keyof UserApi>;
interface UserFrontendAPIStructure {
  getUserStatistics(...omit: any[]): Promise<UserStatisticsResponse>;
}

export type UserBackendAPI = Pick<Modify<UserApi, UserBackendAPIStructure>, keyof UserApi>;
interface UserBackendAPIStructure {
  getUserStatistics(...omit: any[]): Promise<UserStatisticsResponse>;
}

export const USER_ROUTES: APIRoutes<UserApi> = {
  getUserStatistics: (id: string) => `/users/${id}/statistics`
};
