import { ValidationError } from "class-validator";

import { PostDraft } from "../post/post.api";

function dfs(src: ValidationError[], dest: Partial<MessagesConstraints>): void {
  for (const validationError of src) {
    const property: keyof MessagesConstraints = validationError.property as keyof MessagesConstraints;
    dest[property] = validationError.children?.length ? {} : validationError.constraints;

    if (validationError.children?.length) {
      dfs(validationError.children, dest[property] as unknown as Partial<MessagesConstraints>);
    }
  }
}

export interface MessagesConstraints {
  post: { [key in keyof Partial<PostDraft>]: { [validationConstraint: string]: string } };
}

export class MessageFormatter {
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types,@typescript-eslint/explicit-function-return-type
  public static format(validationErrors: ValidationError[]) {
    const dest: Partial<MessagesConstraints> = {};
    dfs(validationErrors, dest);
    return dest;
  }
}
